<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>ZK Cinema</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="ZK.css">
</head>
<body>
<?php 
require_once 'connect.php';

$tim_kiem = '';
if(isset($_GET['q'])){
	$tim_kiem = $_GET['q'];
}
$sql = "SELECT * FROM tbl_movie
	WHERE movie_name LIKE '%$tim_kiem%' 
	ORDER BY movie_id DESC";
$array = mysqli_query($connect,$sql);
$dem = mysqli_num_rows($array);

?>
<div id="all">
	<?php require_once 'check_user.php' ?>
	<div id="content">
		<?php if ($dem == 0) {
			echo '<div style="margin: 5%;">Không có kết quả cho <b>'. $tim_kiem .'</b></div>';
		}else{
			echo '<div class="movie_list">
				<button class="button" onclick="toggle_movie()">Phim</button>
				<div id="toggle_movie">
					<div class="movie">';
					foreach ($array as $each){
					echo '<a href="detail.php?movie_id='.$each['movie_id'].'">
					<div class="movie_holder">
					<img src="images/uploaded/' . $each['image'] . '">
						<div class="info">
							<h1>'.$each['movie_name'].'</h1>
							<p>'.$each['description'].'</p>
						</div>
					</div>
					</a>';
					}
					echo '</div>
				</div>
			</div>';
		} ?>

	</div>
</div>
<script src="signup_login.js"></script>
<script src="toggle_search.js"></script>
<?php mysqli_close($connect); ?>
</body>
</html>