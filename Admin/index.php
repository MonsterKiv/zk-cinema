<?php require_once 'check_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Quản lý</title>
	<style type="text/css">
		/*==Reset CSS==*/
		* {
		  margin: 0;
		  padding: 0;
		}
		#menu{
			background-color: #1F568B;
			color: white;
		}

		/*==Style cơ bản cho website==*/
		body {
		  font-family: sans-serif;
		  color: #333;
		}

		/*==Style cho menu===*/
		#menu ul {
		  background: #1F568B;
		  list-style-type: none;
		  text-align: center;
		}
		#menu li {
		  color: #f1f1f1;
		  display: inline-block;
		  width: 150px;
		  height: 40px;
		  line-height: 40px;
		  margin-left: -5px;
		}
		#menu a {
		  text-decoration: none;
		  color: #fff;
		  display: block;
		}
		#menu a:hover {
		  background: #F1F1F1;
		  color: #333;
		}
	</style>
</head>
<body>
<div id="menu">
Xin chào
<?php 
if($_SESSION['lvl']=='0'){
	echo "Admin";
}
else{
	echo "Super Admin";
}

?>
&nbsp;
<?php
echo $_SESSION['username_admin'];
echo '<a href="logout.php">Đăng xuất</a>';
?>
	<ul>
		<?php IF($_SESSION['lvl']=='1'){
		echo '<li>
		<a href="Manage_Admin/">
			Quản lý Admin
		</a>
		</li>
		<li>
		<a href="Manage_User/">
			Quản lý Khách hàng
		</a>
		</li>';
		}
		?>
		<li>
		<a href="Add_Post/">
			Đăng bài
		</a>
		</li>
		<li>
		<a href="Manage_Ticket/">
			Kiểm duyệt vé
		</a>
		</li>
		<li>
		<a href="Manage_Movie/">
			Quản lý Phim
		</a>
		</li>
		<li>
		<a href="Manage_Screen/">
			Quản lý Phòng chiếu
		</a>
		</li>
		<li>
		<a href="Manage_Shows/">
			Quản lý Suất chiếu
		</a>
		</li>
	<ul>
</div>
</body>
</html>