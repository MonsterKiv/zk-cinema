<?php require_once 'check_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Quản lý phòng chiếu</title>
</head>
<body>
<?php 
if(isset($_GET['notification'])){
	echo $_GET['notification'];
	echo "<br><br>";
}
?>

<?php 
require_once '../../connect.php';
$sql = "SELECT * FROM tbl_screen";
$array = mysqli_query($connect,$sql);
?>

<a href="../">Quay lại Dashboard</a>
<br>
<a href="../logout.php">Logout</a>
<br>
<a href="form_insert.php">Thêm phòng chiếu</a>

<table width="75%" border="1px solid black">
	<tr style="text-align: center;">
		<th>Mã phòng chiếu</th>
		<th>Tên phòng chiếu</th>
		<th>Tổng số ghế</th>
		<th></th>
		<th></th>
	</tr>
	
	<?php foreach ($array as $each): ?>
		<tr>
			<td style="text-align: center;"><?php echo $each['screen_id'] ?></td>
			<td style="text-align: center;"><a href="../Manage_Shows/index.php?screen_id=<?php echo $each['screen_id'] ?>"><?php echo $each['screen_name'] ?></a></td>
			<td style="text-align: center;"><?php echo $each['seats'] ?></td>
			<td style="text-align: center;">
				<a href="form_alter.php?screen_id=<?php echo $each['screen_id'] ?>">Sửa</a>
			</td>
			<td style="text-align: center; background-color: rgba(255,0,0,0.5);">
				<a href="delete.php?screen_id=<?php echo $each['screen_id'] ?>">Xoá</a>
			</td>
		</tr>
	<?php endforeach ?>
</table>

<?php mysqli_close($connect); ?>
</body>
</html>