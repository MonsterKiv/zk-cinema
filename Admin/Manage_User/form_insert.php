<?php require_once 'check_admin.php'; ?>
<?php require_once 'check_super_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Thêm Admin/Super Admin</title>
</head>
<body>
Lưu ý: Tên đăng nhập không thể thay đổi!
<br>
<a href="index.php">Quay lại</a>
<br>
<form action="process_insert.php" method="post">
	Tên đăng nhập
	<input type="text" name="username">
	<br>
	Mật khẩu
	<input type="password" name="pwd">
	<br>
	- Thông tin cá nhân -
	<br>
	Họ Tên
	<input type="text" name="name">
	<br>
	Email
	<input type="email" name="email">
	<br>
	Số Điện Thoại
	<input type="text" name="phone">
	<br>
	Tuổi
	<input type="number" name="age">
	<br>
	Giới Tính
	<input type="radio" name="gender" value="0" checked>Nam
	<input type="radio" name="gender" value="1">Nữ
	<br>
	<button>Thêm</button>
</form>
</body>
</html>