<?php require_once 'check_admin.php'; ?>
<?php require_once 'check_super_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Thông tin chi tiết User</title>
</head>
<body>

<?php 
require_once '../../connect.php';
$user_id = $_GET['user_id'];
$sql = "select * from tbl_login
	where
	user_id = '$user_id'";
$array = mysqli_query($connect,$sql);
?>

<a href="index.php">Quay lại</a>

<table width="75%" border="1px solid black">
	<tr style="text-align: center;">
		<th>User ID</th>
		<th>Họ Tên</th>
		<th>Email</th>
		<th>Điện Thoại</th>
		<th>Tuổi</th>
		<th>Giới Tính</th>
		<th>Sửa</th>
		<th>Xoá</th>
	</tr>
	<?php foreach ($array as $each): ?>
		<tr>
			<td><?php echo $each['user_id'] ?></td>
			<td><?php echo $each['name'] ?></td>
			<td><?php echo $each['email'] ?></td>
			<td><?php echo $each['phone'] ?></td>
			<td><?php echo $each['age'] ?></td>
			<td>
				<?php 
				if($each['gender']=='0'){
					echo "Nam";
				}
				else{
					echo "Nữ";
				}
				?>
			</td>
			<td style="text-align: center;">
				<a href="form_alter.php?user_id=<?php echo $each['user_id'] ?>">Sửa</a>
			</td>
			<td style="text-align: center; background-color: rgba(255,0,0,0.5);">
				<a href="delete.php?user_id=<?php echo $each['user_id'] ?>">Xoá</a>
			</td>
		</tr>
	<?php endforeach ?>
</table>

<?php mysqli_close($connect); ?>
</body>
</html>