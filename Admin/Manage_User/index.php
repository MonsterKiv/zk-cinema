<?php require_once 'check_admin.php'; ?>
<?php require_once 'check_super_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Bảng điều khiển User</title>
</head>
<body>
<?php 
if(isset($_GET['notification'])){
	echo $_GET['notification'];
	echo "<br><br>";
}
?>

<?php 
require_once '../../connect.php';
$sql = "select * from tbl_login";
$array = mysqli_query($connect,$sql);
?>

<a href="../index.php">Quay lại Dashboard</a>
<br>
<a href="../logout.php">Logout</a>
<br>
<a href="form_insert.php">Thêm Tài khoản User</a>

<table width="75%" border="1px solid black">
	<tr style="text-align: center;">
		<th>User ID</th>
		<th>Tên đăng nhập</th>
		<th>Thông tin</th>
		<th>Sửa</th>
		<th>Xoá</th>
	</tr>
	<?php foreach ($array as $each): ?>
		<tr>
			<td><?php echo $each['user_id'] ?></td>
			<td><?php echo $each['username'] ?></td>
			<td style="text-align: center;">
				<a href="detail_index.php?user_id=<?php echo $each['user_id'] ?>">Thông tin</a>
			</td>
			<td style="text-align: center;">
				<a href="form_alter.php?user_id=<?php echo $each['user_id'] ?>">Sửa</a>
			</td>
			<td style="text-align: center; background-color: rgba(255,0,0,0.5);">
				<a href="delete.php?user_id=<?php echo $each['user_id'] ?>">Xoá</a>
			</td>
		</tr>
	<?php endforeach ?>
</table>

<?php mysqli_close($connect); ?>
</body>
</html>