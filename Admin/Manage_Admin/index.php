<?php require_once 'check_admin.php'; ?>
<?php require_once 'check_super_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Bảng điều khiển Admin</title>
</head>
<body>
<?php 
if(isset($_GET['notification'])){
	echo $_GET['notification'];
	echo "<br><br>";
}
?>

<?php 
require_once '../../connect.php';
$sql = "select * from admin";
$array = mysqli_query($connect,$sql);
?>

<a href="../index.php">Quay lại Dashboard</a>
<br>
<a href="../logout.php">Logout</a>
<br>
<a href="form_insert.php">Thêm Admin/Super Admin</a>

<table width="75%" border="1px solid black">
	<tr style="text-align: center;">
		<th>Username</th>
		<th>Số Điện Thoại</th>
		<th>Cấp Bậc</th>
		<th>Sửa</th>
		<th>Xoá</th>
	</tr>
	<?php foreach ($array as $each): ?>
		<tr>
			<td><?php echo $each['username'] ?></td>
			<td><?php echo $each['phone'] ?></td>
			<td>
				<?php 
				if($each['lvl']==0){
					echo "Admin";
				}
				else{
					echo "Super Admin";
				}
				?>
			</td>
			<td style="text-align: center;">
				<a href="form_alter.php?username=<?php echo $each['username'] ?>">Sửa</a>
			</td>
			<td style="text-align: center; background-color: rgba(255,0,0,0.5);">
				<a href="delete.php?username=<?php echo $each['username'] ?>">Xoá</a>
			</td>
		</tr>
	<?php endforeach ?>
</table>

<?php mysqli_close($connect); ?>
</body>
</html>