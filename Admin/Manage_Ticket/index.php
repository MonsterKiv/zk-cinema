<?php require_once 'check_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Kiểm duyệt vé</title>
</head>
<body onload="openDefault()">
<?php 
require_once '../../connect.php';
date_default_timezone_set("Asia/Ho_Chi_Minh");
if(isset($_GET['notification'])){
	echo $_GET['notification'];
	echo "<br><br>";
}
if(isset($_GET['error'])){
	echo $_GET['error'];
	echo "<br><br>";
}
if(isset($_GET['book_id']) && isset($_GET['user_id']) && isset($_GET['time_code'])){
	$book_id = $_GET['book_id'];
	$user_id = $_GET['user_id'];
	$time_code = $_GET['time_code'];
	$time_date_form = date("Y-m-d H:i:s",$_GET['time_code']);
	$sql = "SELECT tbl_booking.book_id ,tbl_movie.movie_name, tbl_booking.ticket_date, tbl_shows.start_date, tbl_showtime.showtime_name, tbl_booking.received_ticket,tbl_booking.number_of_tickets, tbl_login.name, tbl_login.phone, tbl_login.age, tbl_login.gender
	FROM tbl_booking
	JOIN tbl_shows
	ON tbl_booking.show_id = tbl_shows.show_id
	JOIN tbl_login
	ON tbl_booking.user_id = tbl_login.user_id
	JOIN tbl_movie
	ON tbl_shows.movie_id = tbl_movie.movie_id
	JOIN tbl_showtime
	ON tbl_shows.showtime_id = tbl_showtime.showtime_id
	WHERE tbl_booking.status = 1
	AND tbl_booking.book_id = '$book_id'
	AND tbl_booking.user_id = '$user_id'
	AND tbl_booking.ticket_date = '$time_date_form'";
	$array_input_check = mysqli_query($connect,$sql);
	$num_rows_input_check = mysqli_num_rows($array_input_check);
	$fetch_input_check = mysqli_fetch_array($array_input_check);
}else{
	$book_id = $user_id = $time_code = "";
}

$sql = "SELECT tbl_booking.book_id ,tbl_movie.movie_name, tbl_booking.ticket_date, tbl_shows.start_date, tbl_showtime.showtime_name, tbl_booking.received_ticket,tbl_booking.number_of_tickets, tbl_login.name, tbl_login.phone, tbl_login.age, tbl_login.gender
	FROM tbl_booking
    JOIN tbl_shows
	ON tbl_booking.show_id = tbl_shows.show_id
	JOIN tbl_login
	ON tbl_booking.user_id = tbl_login.user_id
	JOIN tbl_movie
	ON tbl_shows.movie_id = tbl_movie.movie_id
	JOIN tbl_showtime
	ON tbl_shows.showtime_id = tbl_showtime.showtime_id
	WHERE tbl_booking.status = 1";
$array_check = mysqli_query($connect,$sql);
$sql = "SELECT tbl_booking.book_id ,tbl_movie.movie_name, tbl_booking.ticket_date, tbl_shows.start_date, tbl_showtime.showtime_name, tbl_booking.number_of_tickets, tbl_booking.received_ticket, tbl_login.name, tbl_login.phone, tbl_login.age, tbl_login.gender
	FROM tbl_booking
    JOIN tbl_shows
	ON tbl_booking.show_id = tbl_shows.show_id
	JOIN tbl_login
	ON tbl_booking.user_id = tbl_login.user_id
	JOIN tbl_movie
	ON tbl_shows.movie_id = tbl_movie.movie_id
	JOIN tbl_showtime
	ON tbl_shows.showtime_id = tbl_showtime.showtime_id
	WHERE tbl_booking.status = 2
	ORDER BY tbl_shows.start_date DESC, tbl_showtime.showtime_name DESC";
$array_accept = mysqli_query($connect,$sql);
$sql = "SELECT tbl_booking.book_id ,tbl_movie.movie_name, tbl_booking.ticket_date, tbl_shows.start_date, tbl_showtime.showtime_name, tbl_booking.number_of_tickets, tbl_login.name, tbl_login.phone, tbl_login.age, tbl_login.gender
	FROM tbl_booking
    JOIN tbl_shows
	ON tbl_booking.show_id = tbl_shows.show_id
	JOIN tbl_login
	ON tbl_booking.user_id = tbl_login.user_id
	JOIN tbl_movie
	ON tbl_shows.movie_id = tbl_movie.movie_id
	JOIN tbl_showtime
	ON tbl_shows.showtime_id = tbl_showtime.showtime_id
	WHERE tbl_booking.status = 0";
$array_decline = mysqli_query($connect,$sql);
?>

<a href="../">Quay lại Dashboard</a>
<br>
<a href="../logout.php">Logout</a>
<br>
<br>

<!-- Tab links -->
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'check_ticket')" id="defaultOpen">Duyệt vé</button>
  <button class="tablinks" onclick="openCity(event, 'waiting_list')">Đã đặt</button>
  <button class="tablinks" onclick="openCity(event, 'accept_list')">Đã thanh toán</button>
  <button class="tablinks" onclick="openCity(event, 'decline_list')">Đã hủy</button>
</div>

<!-- Tab content -->
<div id="check_ticket" class="tabcontent">
<form action="./" method="get">
	<br>
	Nhập mã số đặt vé
	<br>
	<input type="number" name="book_id" placeholder="Mã đơn đặt vé" value="<?php echo $book_id ?>"> - 
	<input type="number" name="user_id" placeholder="Mã khách hàng" value="<?php echo $user_id ?>"> - 
	<input type="number" name="time_code" placeholder="Mã thời gian" value="<?php echo $time_code ?>">
	<button>Kiểm tra</button>
	<br><br>
</form>
<?php 
if (!empty($_GET['book_id'])) {
	if ($num_rows_input_check == 0) {
		echo 'Vé không hợp lệ, vui lòng kiểm tra lại!';
	}else{
		echo '<table width="80%" border="1px solid black">
		<tr style="text-align: center;">
			<th>Tên phim</th>
			<th>Ngày chiếu</th>
			<th>Suất chiếu</th>
			<th>Thời gian đặt vé</th>
			<th>Vé đã duyệt</th>
			<th>Số lượng vé</th>
			<th>Thông tin người đặt</th>
			<th colspan="2">Số vé duyệt</th>
		</tr>
		
		<tr style="text-align: center;">
			<td>'.$fetch_input_check['movie_name'].'</td>
			<td>'.date("d-m-Y", strtotime($fetch_input_check['start_date'])).'</td>
			<td>'.$fetch_input_check['showtime_name'].'</td>
			<td>'.date("d-m-Y, H:i:s", strtotime($fetch_input_check['ticket_date'])).'</td>
			<td>'.$fetch_input_check['received_ticket'].'</td>
			<td>'.$fetch_input_check['number_of_tickets'].'</td>
			<td>
				'.$fetch_input_check['name'].', '.$fetch_input_check['phone'].', '.$fetch_input_check['age'].' tuổi, ';
			if ($fetch_input_check['gender'] == 0) {
			 	echo 'Nam';
			}else{
				echo 'Nữ';
			}
			echo '</td>
			<form action="check_ticket.php?book_id='.$book_id.'&user_id='.$user_id.'&time_code='.$time_code.'" method="post">
			<td>
				<input type="hidden" name="book_id" value="'.$book_id.'">
				<input type="hidden" name="number_of_tickets" value="'.$fetch_input_check['number_of_tickets'].'">
				<input type="number" name="check_ticket">
			</td>
			<td>
				<button>Duyệt</button>
			</td>
			</form>
		</tr>
		</table>';
	}
}
?>
</div>

<div id="waiting_list" class="tabcontent">
<table width="80%" border="1px solid black">
	<tr style="text-align: center;">
		<th>Tên phim</th>
		<th>Ngày chiếu</th>
		<th>Suất chiếu</th>
		<th>Thời gian đặt vé</th>
		<th>Vé đã duyệt</th>
		<th>Số lượng vé</th>
		<th>Thông tin người đặt</th>
		<th colspan="2"></th>
	</tr>
	
	<?php foreach ($array_check as $each): ?>
		<tr style="text-align: center;">
			<td><?php echo $each['movie_name'] ?></td>
			<td><?php echo date("d-m-Y", strtotime($each['start_date'])) ?></td>
			<td><?php echo $each['showtime_name'] ?></td>
			<td><?php echo date("d-m-Y, H:i:s", strtotime($each['ticket_date'])) ?></td>
			<td><?php echo $each['received_ticket'] ?></td>
			<td><?php echo $each['number_of_tickets'] ?></td>
			<td>
				<?php echo $each['name'].', '.$each['phone'].', '.$each['age'].' tuổi, ';
				if ($each['gender'] == 0) {
				 	echo 'Nam';
				}else{
					echo 'Nữ';
				} ?>
			</td>
			<td style="background-color: rgba(0,255,0,0.5);">
				<?php if ($each['received_ticket'] == $each['number_of_tickets']) {
				echo '<a href="accept_ticket.php?book_id='.$each['book_id'].'">Thanh toán</a>';
				} ?>
			</td>
			<td style="background-color: rgba(255,0,0,0.5);">
				<a href="decline_ticket.php?book_id=<?php echo $each['book_id'] ?>">Hủy vé</a>
			</td>
		</tr>
	<?php endforeach ?>
</table>
<?php if (mysqli_num_rows($array_check) != 0) {
	echo '<a href="accept_ticket.php">Thanh toán tất cả</a>
	<br>
	<a href="decline_ticket.php">Hủy tất cả</a>';
} ?>
</div>

<div id="accept_list" class="tabcontent">
<table width="80%" border="1px solid black">
	<tr style="text-align: center;">
		<th>Tên phim</th>
		<th>Ngày chiếu</th>
		<th>Suất chiếu</th>
		<th>Thời gian đặt vé</th>
		<th>Số lượng vé</th>
		<th>Số vé đã duyệt</th>
		<th>Thông tin người đặt</th>
	</tr>
	
	<?php foreach ($array_accept as $each): ?>
		<tr style="text-align: center;">
			<td><?php echo $each['movie_name'] ?></td>
			<td><?php echo date("d-m-Y", strtotime($each['start_date'])) ?></td>
			<td><?php echo $each['showtime_name'] ?></td>
			<td><?php echo date("d-m-Y, H:i:s", strtotime($each['ticket_date'])) ?></td>
			<td><?php echo $each['number_of_tickets'] ?></td>
			<td><?php echo $each['received_ticket'] ?></td>
			<td>
				<?php echo $each['name'] . ', ' . $each['phone'] . ', ' . $each['age'] . ' tuổi, ';
				if ($each['gender'] == 0) {
				 	echo 'Nam';
				}else{
					echo 'Nữ';
				} ?>
			</td>
		</tr>
	<?php endforeach ?>
</table>
</div>

<div id="decline_list" class="tabcontent">
<table width="80%" border="1px solid black">
	<tr style="text-align: center;">
		<th>Tên phim</th>
		<th>Ngày chiếu</th>
		<th>Suất chiếu</th>
		<th>Thời gian đặt vé</th>
		<th>Số lượng vé</th>
		<th>Thông tin người đặt</th>
	</tr>
	
	<?php foreach ($array_decline as $each): ?>
		<tr style="text-align: center;">
			<td><?php echo $each['movie_name'] ?></td>
			<td><?php echo date("d-m-Y", strtotime($each['start_date'])) ?></td>
			<td><?php echo $each['showtime_name'] ?></td>
			<td><?php echo date("d-m-Y, H:i:s", strtotime($each['ticket_date'])) ?></td>
			<td><?php echo $each['number_of_tickets'] ?></td>
			<td>
				<?php echo $each['name'].' , '.$each['phone'].', '.$each['age'].' tuổi, ';
				if ($each['gender'] == 0) {
				 	echo 'Nam';
				}else{
					echo 'Nữ';
				} ?>
			</td>
		</tr>
	<?php endforeach ?>
</table>
</div>

<?php mysqli_close($connect); ?>
<script src="tab.js"></script>
</body>
</html>