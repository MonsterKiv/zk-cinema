<?php require_once 'check_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Quản lý suất chiếu</title>
	<script type="text/javascript">
		function redirect() {
			var val = document.getElementById('select').value;
			location.href = "./?screen_id=" + val;
		}
	</script>
</head>
<body>
<?php 
require_once '../../connect.php';
if(isset($_GET['notification'])){
	echo $_GET['notification'];
	echo "<br><br>";
}
$sql = "SELECT screen_id ,screen_name FROM tbl_screen";
$select_screen = mysqli_query($connect,$sql);

if(isset($_GET['screen_id'])){
	$screen_id = $_GET['screen_id'];
	$sql = "SELECT tbl_shows.show_id, tbl_screen.screen_name, tbl_movie.movie_name, tbl_movie.image, tbl_shows.start_date, tbl_shows.showtime_id, tbl_shows.end_time, tbl_showtime.showtime
		FROM tbl_shows
		JOIN tbl_screen
			ON tbl_shows.screen_id = tbl_screen.screen_id
		JOIN tbl_movie
			ON tbl_shows.movie_id = tbl_movie.movie_id
		JOIN tbl_showtime
			ON tbl_shows.showtime_id = tbl_showtime.showtime_id
		WHERE tbl_screen.screen_id = '$screen_id'
		ORDER BY tbl_shows.start_date DESC, tbl_showtime.showtime DESC";
	$array = mysqli_query($connect,$sql);
	$fetch = mysqli_fetch_array($array);
	$screen_name = $fetch['screen_name'];
	echo '<a href="./">Hiển thị theo tất cả phòng chiếu</a><br>';
	echo '<a href="../Manage_Screen/">Quay lại Phòng chiếu</a>
<br>';

}else{
	$sql = "SELECT tbl_shows.show_id, tbl_screen.screen_name, tbl_movie.movie_name, tbl_movie.image, tbl_shows.start_date, tbl_shows.showtime_id, tbl_shows.end_time, tbl_showtime.showtime
	FROM tbl_shows
	JOIN tbl_screen
		ON tbl_shows.screen_id = tbl_screen.screen_id
	JOIN tbl_movie
		ON tbl_shows.movie_id = tbl_movie.movie_id
	JOIN tbl_showtime
		ON tbl_shows.showtime_id = tbl_showtime.showtime_id
	ORDER BY tbl_shows.start_date DESC, tbl_showtime.showtime DESC, tbl_screen.screen_name DESC";
	$array = mysqli_query($connect,$sql);
	$screen_name = 'Tất cả';
}
?>

<a href="../">Quay lại Dashboard</a>
<br>
<a href="form_insert.php">Thêm suất chiếu</a>
<br>
<a href="../logout.php">Logout</a>
<br>
<p>Phòng chiếu: <b><?php echo "$screen_name"; ?></b></p>
<br>
Chọn phòng chiếu để hiển thị:
<form>
	<select onchange="redirect()" id="select">
		<option selected>Chọn phòng chiếu</option>
		<?php foreach ($select_screen as $each): ?>
			<option value="<?php echo $each['screen_id'] ?>">
				<?php echo $each['screen_name'] ?>
			</option>
		<?php endforeach ?>
	</select>
</form>
<table width="75%" border="1px solid black">
	<tr style="text-align: center;">
		<th>Mã suất chiếu</th>
		<th>Phim</th>
		<th>Ngày chiếu</th>
		<th>Thời gian bắt đầu</th>
		<th>Thời gian kết thúc</th>
		<th></th>
		<th></th>
	</tr>
	<?php foreach ($array as $each): ?>
		<tr>
			<td><?php echo $each['show_id'] ?></td>
			<td><img src="../../images/uploaded/<?php echo $each ['image'] ?>" height="175">&nbsp<?php echo $each['movie_name'] ?></td>
			<td><?php echo $each['start_date'] ?></td>
			<td><?php echo $each['showtime'] ?></td>
			<td><?php echo $each['end_time'] ?></td>
			<td style="text-align: center;">
				<a href="form_alter.php?show_id=<?php echo $each['show_id'] ?>">Sửa</a>
			</td>
			<td style="text-align: center; background-color: rgba(255,0,0,0.5);">
				<a href="delete.php?show_id=<?php echo $each['show_id'] ?>">Xoá</a>
			</td>
		</tr>
	<?php endforeach ?>
</table>

<?php mysqli_close($connect); ?>
</body>
</html>