<?php require_once 'check_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Sửa suất chiếu</title>
</head>
<body>
<?php 
require_once '../../connect.php';
$show_id = $_GET['show_id'];
$sql = "SELECT * FROM tbl_shows WHERE show_id = '$show_id'";
$array = mysqli_query($connect,$sql);
$each = mysqli_fetch_array($array);

$sql = "SELECT screen_id, screen_name
	FROM tbl_screen";
$array_screen_all = mysqli_query($connect,$sql);

$sql = "SELECT movie_id, movie_name
	FROM tbl_movie
	WHERE status_movie = 1";
$array_movie_all = mysqli_query($connect,$sql);

$sql = "SELECT showtime_id, showtime_name
	FROM tbl_showtime";
$array_showtime_all = mysqli_query($connect,$sql);

$sql = "SELECT screen_name
	FROM tbl_screen
	WHERE screen_id = {$each['screen_id']}";
$array_screen = mysqli_query($connect,$sql);
$each_screen = mysqli_fetch_array($array_screen);

$sql = "SELECT movie_name
	FROM tbl_movie
	WHERE movie_id = {$each['movie_id']}";
$array_movie = mysqli_query($connect,$sql);
$each_movie = mysqli_fetch_array($array_movie);

$sql = "SELECT showtime_name
	FROM tbl_showtime
	WHERE showtime_id = {$each['showtime_id']}";
$array_showtime = mysqli_query($connect,$sql);
$each_showtime = mysqli_fetch_array($array_showtime);
?>

<a href="index.php">Quay lại</a>
<br>
<form action="process_alter.php" method="post">
	<input type="hidden" name="show_id" value="<?php echo $each['show_id'] ?>">
	Phòng chiếu
	<select name="screen_id">
		<?php foreach ($array_screen_all as $each_screen_all): ?>
			<option value="<?php echo $each_screen_all['screen_id'] ?>" <?php IF($each_screen_all['screen_id']==$each['screen_id']){echo "selected";} ?>>
				<?php echo $each_screen_all['screen_name'] ?>
			</option>
		<?php endforeach ?>
	</select>
	<br>
	Phim
	<select name="movie_id">
		<?php foreach ($array_movie_all as $each_movie_all): ?>
			<option value="<?php echo $each_movie_all['movie_id'] ?>" <?php IF($each_movie_all['movie_id']==$each['movie_id']){echo "selected";} ?>>
				<?php echo $each_movie_all['movie_name'] ?>
			</option>
		<?php endforeach ?>
	</select>
	<br>
	Ngày khởi chiếu
	<input type="date" name="start_date" value="<?php echo $each['start_date'] ?>">
	<br>
	Giờ bắt đầu
	<select name="showtime_id">
		<?php foreach ($array_showtime_all as $each_showtime_all): ?>
			<option value="<?php echo $each_showtime_all['showtime_id'] ?>" <?php IF($each_showtime_all['showtime_id']==$each['showtime_id']){echo "selected";} ?>>
				<?php echo $each_showtime_all['showtime_name'] ?>
			</option>
		<?php endforeach ?>
	</select>
	<br>
	Giờ kết thúc
	<input type="time" name="end_time" value="<?php echo $each['end_time'] ?>">
	<br>
	<button>Sửa</button>
</form>
</body>
</html>