<?php require_once 'check_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Thêm suất chiếu</title>
</head>
<body>
<?php 
if(isset($_GET['notification'])){
	echo $_GET['notification'];
	echo "<br><br>";
}
require_once '../../connect.php';
$sql = "SELECT screen_id, screen_name
	FROM tbl_screen";
$array_screen = mysqli_query($connect,$sql);

$sql = "SELECT movie_id, movie_name, duration_in_time
	FROM tbl_movie
	WHERE status_movie = 1";
$array_movie = mysqli_query($connect,$sql);

$sql = "SELECT showtime_id, showtime_name
	FROM tbl_showtime";
$array_showtime = mysqli_query($connect,$sql);

date_default_timezone_set("Asia/Ho_Chi_Minh");
mysqli_close($connect);
?>

<a href="./">Quay lại</a>
<br>
<form action="process_insert.php" method="post">
	Phòng chiếu
	<select name="screen_id">
		<?php foreach ($array_screen as $each): ?>
			<option value="<?php echo $each['screen_id'] ?>">
				<?php echo $each['screen_name'] ?>
			</option>
		<?php endforeach ?>
	</select>
	<br>
	Phim
	<select name="movie_id">
		<?php foreach ($array_movie as $each): ?>
			<option value="<?php echo $each['movie_id'] ?>">
				<?php echo $each['movie_name'] ?>
			</option>
		<?php endforeach ?>
	</select>
	<br>
	Ngày chiếu
	<input type="date" name="start_date" value="<?php echo date('Y-m-d') ?>" min="<?php echo date('Y-m-d') ?>">
	<br>
	Giờ bắt đầu
	<select name="showtime_id">
		<?php foreach ($array_showtime as $each): ?>
			<option value="<?php echo $each['showtime_id'] ?>">
				<?php echo $each['showtime_name'] ?>
			</option>
		<?php endforeach ?>
	</select>
	<br>
	<button>Thêm</button>
</form>
</body>
</html>