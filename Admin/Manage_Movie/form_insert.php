<?php require_once 'check_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Thêm phim</title>
</head>
<body>
<a href="index.php">Quay lại</a>
<br>
<form action="process_insert.php" method="post" enctype="multipart/form-data">
	Tên phim
	<input type="text" name="movie_name">
	<br>
	Mô tả
	<textarea name="description"></textarea>
	<br>
	Ngày phát hành
	<input type="date" name="release_date">
	<br>
	Thời lượng (phút)
	<input type="number" name="duration">
	<br>
	Chọn ảnh để upload
    <input type="file" name="image" accept="image/*">
	<br>
	Độ phân giải đề nghị: 1000x560px
	<br>
	Link Trailer
	<input type="text" name="video_url">
	<br>
	Giá vé
	<input type="number" name="price" min="0">
	<br>
	Trạng thái
	<input type="radio" name="status_movie" value="0">Không chiếu
	<input type="radio" name="status_movie" value="1">Đang chiếu
	<br>
	<button>Thêm</button>
</form>
</body>
</html>