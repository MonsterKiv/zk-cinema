<?php require_once 'check_admin.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Quản lý phim</title>
</head>
<body>
<?php 
if(isset($_GET['notification'])){
	echo $_GET['notification'];
	echo "<br><br>";
}
?>

<?php 
require_once '../../connect.php';
$sql = "SELECT * FROM tbl_movie";
$array = mysqli_query($connect,$sql);
?>

<a href="../">Quay lại Dashboard</a>
<br>
<a href="../logout.php">Logout</a>
<br>
<a href="form_insert.php">Thêm Phim</a>

<table width="75%" border="1px solid black">
	<tr style="text-align: center;">
		<th>Mã phim</th>
		<th>Tên phim</th>
		<th>Mô tả</th>
		<th>Ngày phát hành</th>
		<th>Thời lượng (phút)</th>
		<th>Ảnh</th>
		<th>URL Trailer</th>
		<th>Giá vé</th>
		<th>Trạng thái</th>
		<th></th>
		<th></th>
	</tr>
	<?php foreach ($array as $each): ?>
		<tr>
			<td><?php echo $each['movie_id'] ?></td>
			<td><?php echo $each['movie_name'] ?></td>
			<td><?php echo $each['description'] ?></td>
			<td><?php echo $each['release_date'] ?></td>
			<td><?php echo $each['duration'] ?></td>
			<td><img src="../../images/uploaded/<?php echo $each['image'] ?>" height='200px'></td>
			<td><a href="<?php echo $each['video_url'] ?>">Link</a></td>
			<td><?php echo $each['price'] ?></td>
			<td>
				<?php 
				if($each['status_movie']==0){
					echo "Không chiếu";
				}
				else{
					echo "Đang chiếu";
				}
				?>
			</td>
			<td style="text-align: center;">
				<a href="form_alter.php?movie_id=<?php echo $each['movie_id'] ?>">Sửa</a>
			</td>
			<td style="text-align: center; background-color: rgba(255,0,0,0.5);">
				<a href="delete.php?movie_id=<?php echo $each['movie_id'] ?>">Xoá</a>
			</td>
		</tr>
	<?php endforeach ?>
</table>

<?php mysqli_close($connect); ?>
</body>
</html>