<?php
session_start();
if(!isset($_SESSION['user_id'])){
	header('location:index.php?error=Đăng nhập trước đã nhé!');
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Đặt vé</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="ZK.css">
</head>
<body onload="calculate()">
<div id="all">
<?php 
if(isset($_GET['error'])){
	echo '<div class="alert">
	<span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
	<center><strong>' . $_GET['error'] . '</strong></center>
</div>';
}
?>
	<?php require_once 'check_user.php' ?>
	<div id="content" style="padding-top: 50px; padding-left: 5%;">
<?php require_once 'form_buy_ticket.php' ?>
	</div>
</div>
<script src="signup_login.js"></script>
<script src="calculate_ticket.js"></script>
<script src="custom_select.js"></script>
<script src="validate.js"></script>
<?php mysqli_close($connect); ?>
</body>
</html>