<?php 
require_once 'connect.php';
date_default_timezone_set("Asia/Ho_Chi_Minh");
$user_id = $_SESSION['user_id'];
$sql = "SELECT book_id
	FROM tbl_booking
	WHERE user_id = '$user_id'
	AND status = 1";
$array_check = mysqli_query($connect,$sql);
$isset_booking = mysqli_num_rows($array_check);
if ($isset_booking == 0) {
	$sql = "SELECT DISTINCT SUBSTRING(tbl_movie.movie_name,1,20) AS movie_name, tbl_shows.movie_id
		FROM tbl_shows
		JOIN tbl_movie
		ON tbl_shows.movie_id = tbl_movie.movie_id
		JOIN tbl_showtime
		ON tbl_shows.showtime_id = tbl_showtime.showtime_id
		WHERE tbl_movie.status_movie = 1
		AND (tbl_shows.start_date BETWEEN ADDDATE(CONVERT(NOW(), date), INTERVAL 1 DAY) AND ADDDATE(CONVERT(NOW(), date), INTERVAL 7 DAY)
			OR (tbl_showtime.showtime_name > CONVERT(NOW(), time) AND (tbl_shows.start_date = CONVERT(NOW(), date))))";
	$array_movie = mysqli_query($connect,$sql);

	if(isset($_GET['movie_id'])){
		$movie_id = $_GET['movie_id'];
		$sql = "SELECT DISTINCT tbl_shows.start_date, tbl_movie.movie_name, tbl_movie.image
		FROM tbl_shows
		JOIN tbl_movie
		ON tbl_shows.movie_id = tbl_movie.movie_id
		JOIN tbl_showtime
		WHERE tbl_shows.movie_id = '$movie_id'
		AND (tbl_shows.start_date BETWEEN ADDDATE(CONVERT(NOW(), date), INTERVAL 1 DAY) AND ADDDATE(CONVERT(NOW(), date), INTERVAL 7 DAY)
			OR (tbl_showtime.showtime_name > CONVERT(NOW(), time) AND (tbl_shows.start_date = CONVERT(NOW(), date))))
		ORDER BY start_date DESC";
	    $array_showdays = mysqli_query($connect,$sql);
	    $image_showdays = mysqli_fetch_array($array_showdays);
	}
	if(isset($_GET['show_id'])){
		$show_id = $_GET['show_id'];
		$sql = "SELECT tbl_shows.start_date, tbl_showtime.showtime_name, tbl_movie.image, tbl_movie.movie_name, tbl_movie.price FROM tbl_shows
		JOIN tbl_movie
		ON tbl_shows.movie_id = tbl_movie.movie_id
		JOIN tbl_showtime
		ON tbl_shows.showtime_id = tbl_showtime.showtime_id
		WHERE tbl_shows.show_id = '$show_id'";
		$array_selected_show = mysqli_query($connect,$sql);
		$selected_show = mysqli_fetch_array($array_selected_show);
	}

	/* Hiển thị từng bước */
	/* Chọn phim */

	if (!isset($_GET['movie_id']) && !isset($_GET['show_id'])){
		echo '<form action="" method="get">
			<table align="center" border="1px solid black">
				<tr>
					<td align="right">Chọn phim</td>
					<td>
					<div class="custom-select">
					<select name="movie_id">
						<option value="notchoose">Lựa chọn</option>';
		foreach ($array_movie as $each) {
			echo '<option value="' . $each['movie_id'] . '">' . $each['movie_name'] . '</option>';
		}
		echo '</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button class="button" style="margin-left: auto; margin-right: auto; display: block;">Tiếp theo</button>
					</td>
				</tr>
			</table>
			</div>
			</form>';

	/* Báo lỗi nếu chưa chọn phim */

	}elseif (isset($_GET['movie_id']) && $movie_id == 'notchoose') {
		header('location:buy_ticket.php?error=Hãy chọn phim trước');

	/* Chọn suất chiếu */

	}elseif (isset($_GET['movie_id']) && $movie_id != 'notchoose' && !isset($_GET['show_id'])){
		echo '
		<p>Phim đã chọn: <b>'. $image_showdays['movie_name'] .'</b></p>
		<img src="images/uploaded/'.$image_showdays['image'].'" width="40%" class="center">';
		echo '<form action="" method="get">
			<input type="hidden" name="movie_id" value="' . $movie_id . '">
			<table align="center" border="1px solid black">';
		foreach ($array_showdays as $each) {
			$start_date = $each['start_date'];

			$sql = "SELECT tbl_shows.show_id, tbl_shows.screen_id, tbl_showtime.showtime_name
			FROM tbl_showtime
			JOIN tbl_shows
			ON tbl_showtime.showtime_id = tbl_shows.showtime_id
			WHERE tbl_shows.movie_id = '$movie_id' AND tbl_shows.start_date = '$start_date'
			ORDER BY tbl_showtime.showtime_name ASC";
			$array_showtime_name = mysqli_query($connect,$sql);

			echo '<tr>
			<td align="center">' . $start_date . '</td>
					<td>';

			foreach ($array_showtime_name as $key) {
				$temp_show_id = $key['show_id'];
				$temp_screen_id = $key['screen_id'];

				$sql_temp_check = "SELECT book_id FROM tbl_booking
					WHERE show_id = '$temp_show_id'";

				$temp_query_x = mysqli_query($connect,$sql_temp_check);
				$temp_fetch_x = mysqli_fetch_array($temp_query_x);

				if (is_null($temp_fetch_x)) {
					$sql_temp = "SELECT seats
							FROM tbl_screen
							WHERE screen_id = '$temp_screen_id'";
					$temp_query = mysqli_query($connect,$sql_temp);
					$fetch_query = mysqli_fetch_array($temp_query);

					echo '<input type="radio" name="show_id" value="'.$temp_show_id.'" id="radio'.$temp_show_id.'"><label for="radio'.$temp_show_id.'">'.$key['showtime_name'].'  (Còn '.$fetch_query['seats'].' vé)</label><br>';
				}else{
					$temp_book_id = $temp_fetch_x['book_id'];
					$sql_temp = "SELECT seats
							FROM tbl_screen
							WHERE screen_id = '$temp_screen_id'";
					$temp_query = mysqli_query($connect,$sql_temp);
					$fetch_query = mysqli_fetch_array($temp_query);
					$temp_seats = $fetch_query['seats'];

					$sql_temp = "SELECT '$temp_seats'-SUM(tbl_booking.number_of_tickets) AS ticket_left
							FROM tbl_booking
						    JOIN tbl_shows
						    ON tbl_booking.show_id = tbl_shows.show_id
						    JOIN tbl_screen
						    ON tbl_shows.screen_id = tbl_screen.screen_id
						    WHERE tbl_shows.show_id = '$temp_show_id'";
					$temp_query = mysqli_query($connect,$sql_temp);
					$fetch_query = mysqli_fetch_array($temp_query);
					if ($fetch_query['ticket_left'] > 0) {
						echo '<input type="radio" name="show_id" value="'.$temp_show_id.'" id="radio'.$temp_show_id.'"><label for="radio'.$temp_show_id.'">'.$key['showtime_name'].' (Còn '.$fetch_query['ticket_left'].' vé)</label><br>';
					}else{
						echo '<input type="radio" name="show_id" value="'.$temp_show_id.'" id="radio'.$temp_show_id.'" disabled><label for="radio'.$temp_show_id.'">'.$key['showtime_name'].' (Đã hết vé)</label><br>';
					}
				}
			}
		}
		echo '</td>
				</tr>
				<tr>
					<td><a href="buy_ticket.php" class="button back">Quay lại<a></td>
					<td><button class="button" style="margin-left: auto; margin-right: auto; display: block;">Tiếp theo</button></td>
				</tr>
			</table>
			</form>';

	/* Check vé - Xác nhận */

	}else{
		echo '<div class="ticket">
				<div>
					<div class="ticket_hole top_left"></div>
					<div class="ticket_hole top_center"></div>
					<div class="ticket_hole top_right"></div>
					<div class="ticket_hole bottom_left"></div>
					<div class="ticket_hole bottom_right"></div>
				</div>
				<div class="title">
					<p>ZK Cinema</p>
					<h3>'. $selected_show['movie_name'] .'</h3>
				</div>
				<img src="images/uploaded/'. $selected_show['image'] .'">
				<div class="ticket_info">
					<table>
					<tr>
						<td class="ticket_label">NGÀY</td>
						<td class="ticket_label">THỜI GIAN</td>
						<td class="ticket_label">GIÁ VÉ</td>
					</tr>
					<tr>
						<td class="ticket_details">'. date("d-m-Y", strtotime($selected_show['start_date'])) .'</td>
						<td class="ticket_details">'. $selected_show['showtime_name'] .'</td>
						<td class="ticket_details">'. number_format($selected_show['price']) .'</td>
					</tr>
					</table>
				</div>
				<img src="images/ticket_barcode.png" id="barcode" class="center">
			</div>
		<div style="float: left; margin-left: 15%; margin-top: 20%;">
		<table border="1px solid black">
		<form name="buy_ticket" action="process_booking.php?movie_id='.$movie_id.'" onsubmit="return validate_buy_ticket()" method="post">
			<input type="hidden" id="price" name="ticket_price" value="' . $selected_show['price'] . '">
		<tr align="center">
			<td colspan="2">Tối đa <i>5 vé / 1 lần đặt</i></td>
		</tr>
		<tr>
			<td align="right">Số lượng vé</td>
			<td align="left"><div id="change_bg"><input type="number" min="1" max="5" name="number_of_tickets" id="number_of_tickets" value="1" oninput="calculate()" style="width: 80px; border: none;"></div></td>
		</tr>
		<tr>
			<th align="right">Tổng</th>
			<th align="left" id="total"></th>
		</tr>
		<tr>
			<input type="hidden" name="user_id" value="' . $_SESSION['user_id'] .'">
			<input type="hidden" name="show_id" value="' . $show_id .'">
			<input type="hidden" name="status" value="1">
			<td><a href="buy_ticket.php?movie_id=' . $movie_id . '" class="button back">Quay lại</td>
			<td><button class="button" style="margin-left: auto; margin-right: auto; display: block;">Đặt vé</button></td>
		</tr>
		</form>
		</table>
		</div>';
	}
}else{
	header('location:index.php?error=Vẫn còn đơn đặt vé đang duyệt, vui lòng chờ để tiếp tục đặt vé!');
}
?>