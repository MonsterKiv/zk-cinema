<?php 
session_start();

date_default_timezone_set("Asia/Ho_Chi_Minh");
$user_id = $_POST['user_id'];
$show_id = $_POST['show_id'];
$ticket_price = $_POST['ticket_price'];
$ticket_date = date("Y-m-d H:i:s");
$number_of_tickets = $_POST['number_of_tickets'];
$status = $_POST['status'];
$movie_id = $_GET['movie_id'];

require_once 'connect.php';

$sql = "SELECT tbl_screen.seats-SUM(tbl_booking.number_of_tickets) AS ticket_left
	FROM tbl_booking
    JOIN tbl_shows
    ON tbl_booking.show_id = tbl_shows.show_id
    JOIN tbl_screen
    ON tbl_shows.screen_id = tbl_screen.screen_id
    WHERE tbl_shows.show_id = '$show_id'
    AND (tbl_booking.status = 1 OR tbl_booking.status = 2)";
$array = mysqli_query($connect,$sql);
$ticket_left = mysqli_fetch_array($array);

if (is_null($ticket_left['ticket_left'])){
	$sql = "SELECT tbl_screen.seats
		FROM tbl_shows
		JOIN tbl_screen
		ON tbl_shows.screen_id = tbl_screen.screen_id
		WHERE tbl_shows.show_id = '$show_id'";
	$array = mysqli_query($connect,$sql);
	$seats = mysqli_fetch_array($array);
	$num_ticket_left = $seats['seats'];
}else {
	$num_ticket_left = $ticket_left['ticket_left'];
}

if ($num_ticket_left >= $number_of_tickets) {
	$sql = "INSERT INTO tbl_booking(user_id, show_id, ticket_price, ticket_date, number_of_tickets, status)
		VALUES ('$user_id', '$show_id', '$ticket_price', '$ticket_date', '$number_of_tickets', '$status')";
	mysqli_query($connect,$sql);
	mysqli_close();
	header('location:index.php?notification=Đặt vé thành công!');
} else{
	mysqli_close();
	header('location:buy_ticket.php?movie_id='.$movie_id.'&error=Vé cho suất chiều này đã hết, mời chọn suất chiếu khác');
}

?>