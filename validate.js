function validate_login() {
	// Get Login Info
	var x = document.forms["login"]["username"].value;
	var y = document.forms["login"]["pwd"].value;

	// Check each one, if wrong, change the border to red and check++ to return false. Else, return border back to original
	var check = 0;
	if (x == "") {
		document.getElementById("login_username").style.border = "2px solid red";
		check++;
	}else{
		document.getElementById("login_username").style.border = "1px solid black";
	}

	if (y == "") {
		document.getElementById("login_pwd").style.border = "2px solid red";
		check++;
	}else{
		document.getElementById("login_pwd").style.border = "1px solid black";
	}

	if (check !== 0) {
		return false;
	}else{
		return true;
	}
}

function validate_signup() {
	// Get Signup Info
	var a = document.forms["signup"]["username"].value;
	var b = document.forms["signup"]["pwd"].value;
	var c = document.forms["signup"]["check_pwd"].value;
	var d = document.forms["signup"]["name"].value;
	var e = document.forms["signup"]["email"].value;
	var f = document.forms["signup"]["phone"].value;
	var g = document.forms["signup"]["age"].value;

	// Validate
	var regex_username = /^[a-z0-9_]{4,32}$/gm;
	var username = a.match(regex_username);

	var regex_pwd = /^[a-zA-Z\d]{6,32}$/gm;
	var pwd = b.match(regex_pwd);

	var regex_name = /^[A-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴÝỶỸửữựỳỵỷỹ][a-zàáâãèéêìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểễệỉịọỏốồổỗộớờởỡợụủứừ]*( [A-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴÝỶỸửữựỳỵỷỹ][a-zàáâãèéêìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểễệỉịọỏốồổỗộớờởỡợụủứừ]*)+$/gm;
	var name = d.match(regex_name);

	var regex_email = /^[a-z][a-z0-9]{5,}@([a-z]+\.)+[a-z]+$/gm;
	var email = e.match(regex_email);

	var regex_phone = /^0[0-9]{9}$/gm;
	var phone = f.match(regex_phone);

	var regex_age = /(^[1-9][0-9]$|^[1-9]$)/gm;
	var age = g.match(regex_age);

	// Check each one, if wrong, change the border to red and count++ to return false. Else, return border back to original
	var count = 0;
	if (a == "" || a != username) {
		document.getElementById("create_username").style.border = "2px solid red";
		count++;
	}else{
		document.getElementById("create_username").style.border = "1px solid black";
	}

	if (b == "" || b != pwd) {
		document.getElementById("create_pwd").style.border = "2px solid red";
		count++;
	}else{
		document.getElementById("create_pwd").style.border = "1px solid black";
	}

	if (c == "" || c != pwd) {
		document.getElementById("create_check_pwd").style.border = "2px solid red";
		count++;
	}else{
		document.getElementById("create_check_pwd").style.border = "1px solid black";
	}

	if (d == "" || d != name) {
		document.getElementById("create_name").style.border = "2px solid red";
		count++;
	}else{
		document.getElementById("create_name").style.border = "1px solid black";
	}

	if (e == "" || e != email) {
		document.getElementById("create_email").style.border = "2px solid red";
		count++;
	}else{
		document.getElementById("create_email").style.border = "1px solid black";
	}

	if (f == "" || f != phone) {
		document.getElementById("create_phone").style.border = "2px solid red";
		count++;
	}else{
		document.getElementById("create_phone").style.border = "1px solid black";
	}

	if (g == "" || g != age) {
		document.getElementById("create_age").style.border = "2px solid red";
		count++;
	}else{
		document.getElementById("create_age").style.border = "1px solid black";
	}

	if (count !== 0) {
		return false;
	}else{
		return true;
	}
}

function validate_buy_ticket() {
	var x = document.forms["buy_ticket"]["number_of_tickets"].value;

	var regex_num_ticket = /^[1-5]$/gm;
	var num_ticket = x.match(regex_num_ticket);

	if (x == "" || x != num_ticket) {
		document.getElementById("change_bg").style.backgroundColor = "rgba(255,0,0,0.65)";
		return false;
	}
}

function validate_edit_info() {
	// Get Login Info
	var a = document.forms["edit_info"]["name"].value;
	var b = document.forms["edit_info"]["email"].value;
	var c = document.forms["edit_info"]["phone"].value;
	var d = document.forms["edit_info"]["age"].value;

	// Validate
	var regex_name = /^[A-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴÝỶỸửữựỳỵỷỹ][a-zàáâãèéêìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểễệỉịọỏốồổỗộớờởỡợụủứừ]*( [A-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴÝỶỸửữựỳỵỷỹ][a-zàáâãèéêìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểễệỉịọỏốồổỗộớờởỡợụủứừ]*)+$/gm;
	var name = a.match(regex_name);

	var regex_email = /^[a-z][a-z0-9]{5,}@([a-z]+\.)+[a-z]+$/gm;
	var email = b.match(regex_email);

	var regex_phone = /^0[0-9]{9}$/gm;
	var phone = c.match(regex_phone);

	var regex_age = /(^[1-9][0-9]$|^[1-9]$)/gm;
	var age = d.match(regex_age);

	// Check each one, if wrong, change the border to red and check++ to return false. Else, return border back to original
	var check = 0;
	if (a == "" || a != name) {
		document.getElementById("edit_name").style.border = "2px solid red";
		check++;
	}else{
		document.getElementById("edit_name").style.border = "1px solid black";
	}

	if (b == "" || b != email) {
		document.getElementById("edit_email").style.border = "2px solid red";
		check++;
	}else{
		document.getElementById("edit_email").style.border = "1px solid black";
	}

	if (c == "" || c != phone) {
		document.getElementById("edit_phone").style.border = "2px solid red";
		check++;
	}else{
		document.getElementById("edit_phone").style.border = "1px solid black";
	}

	if (d == "" || d != age) {
		document.getElementById("edit_age").style.border = "2px solid red";
		check++;
	}else{
		document.getElementById("edit_age").style.border = "1px solid black";
	}

	if (check !== 0) {
		return false;
	}else{
		return true;
	}
}

function validate_password() {
	var a = document.forms["edit_pwd"]["newpwd"].value;
	var b = document.forms["edit_pwd"]["check_newpwd"].value;
	var count = 0;

	var regex_pwd = /^[a-zA-Z\d]{6,32}$/gm;
	var pwd = a.match(regex_pwd);

	if (a == "" || a != pwd) {
		document.getElementById("edit_newpwd").style.border = "2px solid red";
		count++;
	}else{
		document.getElementById("edit_newpwd").style.border = "1px solid black";
	}

	if (b == "" || b != pwd) {
		document.getElementById("edit_check_newpwd").style.border = "2px solid red";
		count++;
	}else{
		document.getElementById("edit_check_newpwd").style.border = "1px solid black";
	}

	if (count !== 0) {
		return false;
	}else{
		return true;
	}
}