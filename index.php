<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>ZK Cinema</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="ZK.css">
</head>
<body>
<?php 
require_once 'connect.php';
$sql = "SELECT * FROM tbl_movie 
	ORDER BY movie_id DESC LIMIT 3";
$array = mysqli_query($connect,$sql);

$sql_x = "SELECT * FROM tbl_post
	JOIN tbl_movie
	ON tbl_post.movie_id = tbl_movie.movie_id
	ORDER BY post_id DESC LIMIT 3";
$array_x = mysqli_query($connect,$sql_x);
?>
<div id="all">
	<?php require_once 'check_user.php' ?>
<?php 
if(isset($_GET['error'])){
	echo '<div class="alert">
	<span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
	<center><strong>' . $_GET['error'] . '</strong></center>
</div>';
}
if(isset($_GET['notification'])){
	echo '<div class="alert success">
	<span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
	<center><strong>' . $_GET['notification'] . '</strong></center>
</div>';
}
?>
	<div id="content">
		<div class="slideshow-container">
			<?php foreach ($array as $each){
			echo '<a href="detail.php?movie_id='.$each['movie_id'].'"><div class="slides fade">
				<img src="images/uploaded/' . $each['image'] . '" style="width:100%; height:405px; position: absolute; top: -5px; z-index: -1; filter: blur(5px);" class="center">
				<img src="images/uploaded/' . $each['image'] . '" style="width:79%; height:400px" class="center">
				<div class="text"><span>' . $each['movie_name'] . '</span></div>
			</div></a>';
			}?>
		</div>
			<br>
		<div style="text-align: center">
			<span class="dot"></span> 
			<span class="dot"></span> 
			<span class="dot"></span> 
		</div>
		<div class="movie_list">
		<div class="topic_title_movie">
			<a href="movie.php">
				<h2>Phim</h2>
			</a>
		</div>
			<div class="movie">
			<?php foreach ($array as $each){
			echo '<a href="detail.php?movie_id='.$each['movie_id'].'">
			<div class="movie_holder">
			<img src="images/uploaded/' . $each['image'] . '">
				<div class="info">
					<h1>'.$each['movie_name'].'</h1>
					<p>'.$each['description'].'</p>
				</div>
			</div>
			</a>';
			}?>
			</div>
		</div>
		<div class="news_list">
		<div class="topic_title_news">
			<a href="news.php">
				<h2>Tin tức</h2>
			</a>
		</div>
			<div class="news">
			<?php foreach ($array_x as $each){
			echo '<a href="detail.php?post_id='.$each['post_id'].'">
			<div class="news_holder">
			<img src="images/uploaded/' . $each['image'] . '">
				<div class="info">
					<h1>'.$each['post_title'].'</h1>
					<p>'.$each['post_content'].'</p>
				</div>
			</div>
			</a>';
			}?>
			</div>
		</div>
	</div>
</div>
<script src="signup_login.js"></script>
<script src="validate.js"></script>
<script src="movie_slideshow.js"></script>
<?php mysqli_close($connect); ?>
</body>
</html>