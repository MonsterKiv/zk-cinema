<?php
session_start();
if(!isset($_SESSION['user_id'])){
	header('location:index.php?error=Đăng nhập trước đã nhé!');
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Thông tin cá nhân</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="ZK.css">
</head>
<body>
<?php 
if(isset($_GET['error'])){
	echo '<div class="alert">
	<span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> 
	<center><strong>' . $_GET['error'] . '</strong></center>
</div>';
}
?>
<div id="all">
<?php require_once 'check_user.php' ?>
	<div id="content" style="padding: 10%; padding-left: 25%">
	<table>
	<form name="edit_pwd" action="alter_password.php" method="post" onsubmit="return validate_password()">
	<tr>
		<td>Mật khẩu hiện tại</td>
		<td><input type="password" name="pwd"></td>
	</tr>
	<tr>
		<td>Mật khẩu mới</td>
		<td><input type="password" name="newpwd" id="edit_newpwd" placeholder="6 - 32 ký tự (chữ và số)"></td>
	</tr>
	<tr>
		<td>Nhập lại mật khẩu mới</td>
		<td><input type="password" name="check_newpwd" id="edit_check_newpwd" placeholder="6 - 32 ký tự (chữ và số)"></td>
	</tr>
		<td colspan="2" align="center"><button class="button">Đổi mật khẩu</button></td>
	</form>
	</table>
	</div>
</div>
<script src="signup_login.js"></script>
<script src="validate.js"></script>
<?php mysqli_close($connect); ?>
</body>
</html>