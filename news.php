<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>ZK Cinema</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="ZK.css">
</head>
<body>
<?php 
require_once 'connect.php';
$sql = "SELECT tbl_post.post_id, tbl_post.post_title, tbl_post.post_content, tbl_movie.image
	FROM tbl_post
	JOIN tbl_movie
	ON tbl_post.movie_id = tbl_movie.movie_id";
$array = mysqli_query($connect,$sql);
$news_list = mysqli_num_rows($array);

$limit = 6;
$present_page = 1;
if (isset($_GET['page'])) {
	$present_page = $_GET['page'];
}
$offset = ($present_page - 1) * $limit;
$num_page = ceil($news_list/$limit);

$sql = "$sql limit $limit offset $offset";
$array = mysqli_query($connect,$sql);

?>
<div id="all">
	<?php require_once 'check_user.php'; ?>
	<div id="content">
		<div class="row">
			<?php foreach ($array as $list) {
				echo '<div class="col"><a href="detail.php?post_id='.$list['post_id'].'">
					<div class="content">
						<img src="images/uploaded/'.$list['image'].'">
						<h2>'.$list['post_title'].'</h2>
						<span>'.$list['post_content'].'</span>
					</div>
				</a></div>';
			} ?>
		</div>
		<div>
			<div class="pagination">
					<?php for($i=1;$i<=$num_page;$i++){
						echo '<a href="?page='.$i.'"';
						if ($present_page == $i) {
							echo ' class="active_page"';
						}
						echo '>'.$i.'</a>';
					} ?>
				</div>
		</div>
	</div>
</div>
<script src="signup_login.js"></script>
<script src="validate.js"></script>
<?php mysqli_close($connect); ?>
</body>
</html>