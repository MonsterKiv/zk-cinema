<?php 
echo '<div id="header">
		<a href="index.php"><img src="images/logo.png" id="icon"></a>
		<div id="menu">
		<ul>
			<li><a href="news.php">TIN TỨC</a></li>
			<li><a href="movie.php">PHIM</a></li>
			<li><a href="buy_ticket.php">ĐẶT VÉ</a></li>
			<li><a href="#">LIÊN HỆ</a></li>
		</ul>
		</div>

		<div id="searchBox">
		<form action="search.php">
			<input type="text" name="q" placeholder="Tên phim..." value="';
if (isset($tim_kiem)){
	echo "$tim_kiem";
}
echo '">
			<button class="button">Tìm kiếm</button>
		</form>
		</div>';
if (!isset($_SESSION['user_id'])){
	echo '<div id="account">
		<button class="button" id="signup_login">Đăng ký/Đăng nhập</button>
	</div>
</div>
<!-- Modal -->
<div id="modal" class="modal">
  <!-- Nội dung Modal -->
	<div class="modal-content">
	    <span class="close">&times;</span>

		<button class="tablink" onclick="openForm(\'Login\', this, \'green\')" id="defaultOpen">Đăng nhập</button>

		<button class="tablink" onclick="openForm(\'Signup\', this, \'green\')">Đăng ký</button>

	    <div id="Login" class="tabcontent">
	    	<br>
			<form name="login" action="process_login.php" onsubmit="return validate_login()" method="post">
			<table align="center">
				<tr>
					<td align="right">Tên đăng nhập</td>
					<td><input type="username" name="username" id="login_username"></td>
				</tr>
				<tr>
					<td align="right">Mật khẩu</td>
					<td><input type="password" name="pwd" id="login_pwd"></td>
				</tr>
			</table>
			<br>
			<button class="button">Đăng nhập</button>
			</form>
		</div>

		<div id="Signup" class="tabcontent">
			<p align="center"><b>Lưu ý: Tên đăng nhập không thể thay đổi sau khi đăng ký!</b></p>
			<form name="signup" action="process_signup.php" onsubmit="return validate_signup()" method="post">
			<br>
			<table align="center">
				<tr>
					<td align="right">Tên đăng nhập</td>
					<td><input type="text" name="username" id="create_username" placeholder="4 - 32 ký tự (chữ, số, và _ )"></td>
				</tr>
				<tr>
					<td align="right">Mật khẩu</td>
					<td><input type="password" name="pwd" id="create_pwd" placeholder="6 - 32 ký tự (chữ và số)"></td>
				</tr>
				<tr>
					<td align="right">Nhập lại mật khẩu</td>
					<td><input type="password" name="check_pwd"
					id="create_check_pwd" placeholder="6 - 32 ký tự (chữ và số)"></td>
				</tr>
				<tr>
					<td align="right">Họ Tên</td>
					<td><input type="text" name="name" id="create_name" placeholder="Nguyễn Văn A"></td>
				</tr>
				<tr>
					<td align="right">Email</td>
					<td><input type="email" name="email" id="create_email" placeholder="example@email.com"></td>
				</tr>
				<tr>
					<td align="right">Số Điện Thoại</td>
					<td><input type="text" name="phone" id="create_phone" placeholder="0987654321"></td>
				</tr>
				<tr>
					<td align="right">Tuổi</td>
					<td><input type="number" name="age" id="create_age" placeholder="1 - 99" min="1" max="99"></td>
				</tr>
				<tr>
					<td align="right">Giới Tính</td>
					<td align="left"><input type="radio" name="gender" value="0" id="male" checked><label for="male">Nam</label>
					<input type="radio" name="gender" value="1" id="female"><label for="female">Nữ</label></td>
				</tr>
			</table>
			<br>
			<button class="button">Tạo và đăng nhập</button>
			</form>
		</div>

	</div>
</div>';
}else{
	echo '<div id="account">
		<div id="name">' . $_SESSION['name'] . '</div>
		<div id="account_options">
			<a href="user_info.php">Thông tin cá nhân</a>
			<a href="change_password.php">Đổi mật khẩu</a>
			<a href="ticket.php">Lịch sử đặt vé</a>
			<a href="logout.php">Đăng xuất</a>
		</div>
	</div>
</div>';
}
?>