<?php
session_start();
if(!isset($_SESSION['user_id'])){
	header('location:index.php?error=Đăng nhập trước đã nhé!');
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Thông tin cá nhân</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="ZK.css">
</head>
<body>
<div id="all">
<?php require_once 'check_user.php' ?>
	<div id="content" style="padding: 10%; padding-left: 25%">
	<table>
	<form name="edit_info" action="alter_infomation.php" method="post" onsubmit="return validate_edit_info()">
	<tr>
		<td>Họ tên</td>
		<td><input type="text" name="name" class="toggle" id="edit_name" value="<?php echo $_SESSION['name'] ?>" disabled></td>
	</tr>
	<tr>
		<td>Email</td>
		<td><input type="text" name="email" class="toggle" id="edit_email" value="<?php echo $_SESSION['email'] ?>" disabled></td>
	</tr>
	<tr>
		<td>Số điện thoại</td>
		<td><input type="text" name="phone" class="toggle" id="edit_phone" value="<?php echo $_SESSION['phone'] ?>" disabled></td>
	</tr>
	<tr>
		<td>Tuổi</td>
		<td><input type="number" name="age" class="toggle" id="edit_age" value="<?php echo $_SESSION['age'] ?>" disabled></td>
	</tr>
	<tr>
		<td>Giới tính</td>
		<td>
			<input type="radio" name="gender" class="toggle" value="0" id="male" <?php if ($_SESSION['gender'] == 0) {
			echo 'checked';
		} ?> disabled><label for="male">Nam</label>
		<input type="radio" name="gender" class="toggle" value="1" id="female"<?php if ($_SESSION['gender'] == 1) {
			echo 'checked';
		} ?> disabled><label for="female">Nữ</label>
		</td>
	</tr>
		<td>
			<label class="switch">
				<input type="checkbox" id="checkbox" onclick="toggle_edit_info()">
				<span class="slider round"></span>
			</label>
		</td>
		<td><button class="button toggle" style="display: none;">Thay đổi thông tin</button><span class="toggle">← Nhấn vào đây để thay đổi thông tin</span></td>
	</form>
	</table>
	</div>
</div>
<script src="signup_login.js"></script>
<script src="toggle_edit_info.js"></script>
<script src="validate.js"></script>
</body>
</html>