<?php
session_start();
if(!isset($_SESSION['user_id'])){
	header('location:index.php?error=Đăng nhập trước đã nhé!');
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lịch sử đặt vé</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="ZK.css">
</head>
<body>
<?php 
require_once 'connect.php';
date_default_timezone_set("Asia/Ho_Chi_Minh");
$current_slide = 0;
$user_id = $_SESSION['user_id'];
$sql = "SELECT tbl_movie.movie_name, tbl_movie.image, tbl_movie.price, tbl_movie.duration, tbl_shows.start_date, tbl_booking.ticket_date,tbl_booking.book_id , tbl_showtime.showtime_name, tbl_booking.number_of_tickets, tbl_booking.status
	FROM tbl_booking
    JOIN tbl_shows
	ON tbl_booking.show_id = tbl_shows.show_id
	JOIN tbl_movie
	ON tbl_shows.movie_id = tbl_movie.movie_id
	JOIN tbl_showtime
	ON tbl_shows.showtime_id = tbl_showtime.showtime_id
	WHERE tbl_booking.user_id = '$user_id'
	ORDER BY tbl_booking.ticket_date DESC";
$array = mysqli_query($connect,$sql);
$num_ticket = mysqli_num_rows($array);

$limit = 5;
$present_page = 1;
if (isset($_GET['page'])) {
	$present_page = $_GET['page'];
}
$offset = ($present_page - 1) * $limit;
$num_page = ceil($num_ticket/$limit);

$sql = "$sql limit $limit offset $offset";
$array = mysqli_query($connect,$sql);

?>
<div id="all">
<?php require_once 'check_user.php' ?>
	<div id="content" style="padding-top: 50px; padding-left: 5%;">
		<div class="view_ticket">
			<?php foreach ($array as $selected_show) {
				if ($selected_show['status'] == 1 || $selected_show['status'] == 2) {
					echo '<div class="ticket">
						<div>
							<div class="ticket_hole top_left"></div>
							<div class="ticket_hole top_center"></div>
							<div class="ticket_hole top_right"></div>
							<div class="ticket_hole bottom_left"></div>
							<div class="ticket_hole bottom_right"></div>
						</div>
						<div class="title">
							<p>ZK Cinema</p>
							<h3>'. $selected_show['movie_name'] .'</h3>
						</div>
						<img src="images/uploaded/'. $selected_show['image'] .'">
						<div class="ticket_info">
							<table>
							<tr>
								<td class="ticket_label">NGÀY</td>
								<td class="ticket_label">THỜI GIAN</td>
								<td class="ticket_label">GIÁ VÉ</td>
							</tr>
							<tr>
								<td class="ticket_details">'. date("d-m-Y", strtotime($selected_show['start_date'])) .'</td>
								<td class="ticket_details">'. $selected_show['showtime_name'] .'</td>
								<td class="ticket_details">'. number_format($selected_show['price']) .'</td>
							</tr>
							</table>
						</div>
						<img src="images/ticket_barcode.png" id="barcode" class="center">
						<div style="text-align: center">
							<span style="letter-spacing: 10px;">'.$selected_show['book_id'].' - '.$user_id.' - '.strtotime($selected_show['ticket_date']).'</span>
						</div>
					</div>';
				}
			} ?>
		</div>
		<table class="except center">
		<tr>
			<th>Tên phim</th>
			<th>Suất chiếu</th>
			<th>Ngày đặt vé</th>
			<th>Số vé</th>
			<th>Trang thái</th>
			<th></th>
		</tr>
		<?php foreach ($array as $ticket){
		echo '<tr>
			<td style="width: 43%">'. $ticket['movie_name'] .'</td>
			<td align="center" style="width: 18%;">'. date("d-m-Y", strtotime($ticket['start_date'])) .'<br>'. $ticket['showtime_name'] .'</td>
			<td align="center" style="width: 18%">'. date("d-m-Y H:i:s", strtotime($ticket['ticket_date'])) .'</td>
			<td align="center" style="width: 10%">'. $ticket['number_of_tickets'] .'</td>
			<td style="width: 10%">';
			if ($ticket['status'] == 0) {
				echo "Đã hủy";
			}elseif ($ticket['status'] == 1) {
				echo "Đang xử lý";
			}elseif ($ticket['status'] == 2) {
				echo "Thành công";
			}
			echo '</td>
			<td>';
			if ($ticket['status'] == 1 || $ticket['status'] == 2) {
				$current_slide++;
				echo '<span class="dot" onclick="currentSlide('.$current_slide.')"></span>';
			}
		echo '</td>
		</tr>';
		} ?>
		<tr align="center">
			<td colspan="6">
				<div class="pagination">
					<?php for($i=1;$i<=$num_page;$i++){
						echo '<a href="?page='.$i.'"';
						if ($present_page == $i) {
							echo ' class="active_page"';
						}
						echo '>'.$i.'</a>';
					} ?>
				</div>
			</td>
		</tr>
		</table>
	</div>
</div>
<script src="signup_login.js"></script>
<script src="ticket_list.js"></script>
<?php mysqli_close($connect); ?>
</body>
</html>