<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>ZK Cinema</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="ZK.css">
</head>
<body>
<?php require_once 'connect.php'; ?>

<div id="all">
<?php require_once 'check_user.php'; ?>
	<div id="content" style="padding-top: 15px; padding-left: 3%; padding-right: 3%;">
		<?php 
		if (isset($_GET['movie_id'])) {
			$movie_id = $_GET['movie_id'];
			$sql = "SELECT * FROM tbl_movie
				WHERE movie_id = $movie_id";
			$array = mysqli_query($connect,$sql);
			$fetch_array = mysqli_fetch_array($array);
			echo '<img src="images/uploaded/'.$fetch_array['image'].'" style="width:50%; float: left">
			<div style="float: left; width: 50%; padding-left: 5%;">
				<h2>'.$fetch_array['movie_name'].'</h2>
				<p>Thời lượng: '.$fetch_array['duration'].' phút</p>
				<p>Ngày khởi chiếu: '.date("d-m-Y",strtotime($fetch_array['release_date'])).'</p>
				<a href="'.$fetch_array['video_url'].'" class="button" style="background-color: darkblue;">Xem trailer</a>';
				if (isset($_SESSION['user_id'])) {
					echo '<a href="buy_ticket.php" class="button">Đặt vé</a>';
				}else{
					echo ' Hãy đăng nhập để đặt vé';
				}
			echo '</div>
			<div style="float: left; width: 95%;">
			<h2>Giới thiệu</h2>
			<p>'.$fetch_array['description'].'</p>
			</div>';
		}elseif (isset($_GET['post_id'])) {
			$post_id = $_GET['post_id'];
			$sql = "SELECT tbl_post.post_title, tbl_post.post_content, tbl_movie.image, tbl_movie.movie_name, tbl_movie.duration, tbl_movie.release_date, tbl_movie.video_url
				FROM tbl_post
				JOIN tbl_movie
				ON tbl_post.movie_id = tbl_movie.movie_id
				WHERE tbl_post.post_id = '$post_id'";
			$array = mysqli_query($connect,$sql);
			$fetch_array = mysqli_fetch_array($array);
			echo '<img src="images/uploaded/'.$fetch_array['image'].'" style="width:50%; float: left">
			<div style="float: left; width: 50%; padding-left: 5%;">
				<h2>'.$fetch_array['movie_name'].'</h2>
				<p>Thời lượng: '.$fetch_array['duration'].' phút</p>
				<p>Ngày khởi chiếu: '.date("d-m-Y",strtotime($fetch_array['release_date'])).'</p>
				<a href="'.$fetch_array['video_url'].'" class="button" style="background-color: darkblue;">Xem trailer</a>';
				if (isset($_SESSION['user_id'])) {
					echo '<a href="buy_ticket.php" class="button">Đặt vé</a>';
				}else{
					echo ' Hãy đăng nhập để đặt vé';
				}
			echo '</div>
			<div style="float: left; width: 95%;">
			<h2>'.$fetch_array['post_title'].'</h2>
			<p>'.$fetch_array['post_content'].'</p>
			</div>';
		}
		?>
	</div>
<script src="signup_login.js"></script>
<script src="validate.js"></script>
</body>
</html>